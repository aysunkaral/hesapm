﻿namespace HesapM
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ekranLabel = new System.Windows.Forms.Label();
            this.sayi1Button = new System.Windows.Forms.Button();
            this.sayi2Button = new System.Windows.Forms.Button();
            this.sayi3Button = new System.Windows.Forms.Button();
            this.sayi6Button = new System.Windows.Forms.Button();
            this.sayi5Button = new System.Windows.Forms.Button();
            this.sayi4Button = new System.Windows.Forms.Button();
            this.eksiButton = new System.Windows.Forms.Button();
            this.artiButton = new System.Windows.Forms.Button();
            this.boluButton = new System.Windows.Forms.Button();
            this.carpiButton = new System.Windows.Forms.Button();
            this.esittirButton = new System.Windows.Forms.Button();
            this.sayi0Button = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.sayi9Button = new System.Windows.Forms.Button();
            this.sayi8Button = new System.Windows.Forms.Button();
            this.sayi7Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ekranLabel
            // 
            this.ekranLabel.BackColor = System.Drawing.Color.White;
            this.ekranLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.ekranLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ekranLabel.ForeColor = System.Drawing.Color.Black;
            this.ekranLabel.Location = new System.Drawing.Point(12, 20);
            this.ekranLabel.Name = "ekranLabel";
            this.ekranLabel.Size = new System.Drawing.Size(210, 39);
            this.ekranLabel.TabIndex = 0;
            this.ekranLabel.Text = "0";
            this.ekranLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sayi1Button
            // 
            this.sayi1Button.BackColor = System.Drawing.Color.Wheat;
            this.sayi1Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi1Button.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sayi1Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi1Button.Location = new System.Drawing.Point(12, 67);
            this.sayi1Button.Name = "sayi1Button";
            this.sayi1Button.Size = new System.Drawing.Size(48, 44);
            this.sayi1Button.TabIndex = 1;
            this.sayi1Button.Text = "1";
            this.sayi1Button.UseVisualStyleBackColor = false;
            this.sayi1Button.Click += new System.EventHandler(this.button1_Click);
            // 
            // sayi2Button
            // 
            this.sayi2Button.BackColor = System.Drawing.Color.Wheat;
            this.sayi2Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi2Button.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sayi2Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi2Button.Location = new System.Drawing.Point(66, 67);
            this.sayi2Button.Name = "sayi2Button";
            this.sayi2Button.Size = new System.Drawing.Size(48, 44);
            this.sayi2Button.TabIndex = 2;
            this.sayi2Button.Text = "2";
            this.sayi2Button.UseVisualStyleBackColor = false;
            this.sayi2Button.Click += new System.EventHandler(this.button2_Click);
            // 
            // sayi3Button
            // 
            this.sayi3Button.BackColor = System.Drawing.Color.Wheat;
            this.sayi3Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi3Button.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sayi3Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi3Button.Location = new System.Drawing.Point(120, 67);
            this.sayi3Button.Name = "sayi3Button";
            this.sayi3Button.Size = new System.Drawing.Size(48, 44);
            this.sayi3Button.TabIndex = 3;
            this.sayi3Button.Text = "3";
            this.sayi3Button.UseVisualStyleBackColor = false;
            this.sayi3Button.Click += new System.EventHandler(this.button3_Click);
            // 
            // sayi6Button
            // 
            this.sayi6Button.BackColor = System.Drawing.Color.Wheat;
            this.sayi6Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi6Button.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sayi6Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi6Button.Location = new System.Drawing.Point(120, 117);
            this.sayi6Button.Name = "sayi6Button";
            this.sayi6Button.Size = new System.Drawing.Size(48, 44);
            this.sayi6Button.TabIndex = 6;
            this.sayi6Button.Text = "6";
            this.sayi6Button.UseVisualStyleBackColor = false;
            this.sayi6Button.Click += new System.EventHandler(this.button4_Click);
            // 
            // sayi5Button
            // 
            this.sayi5Button.BackColor = System.Drawing.Color.Wheat;
            this.sayi5Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi5Button.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sayi5Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi5Button.Location = new System.Drawing.Point(66, 117);
            this.sayi5Button.Name = "sayi5Button";
            this.sayi5Button.Size = new System.Drawing.Size(48, 44);
            this.sayi5Button.TabIndex = 5;
            this.sayi5Button.Text = "5";
            this.sayi5Button.UseVisualStyleBackColor = false;
            this.sayi5Button.Click += new System.EventHandler(this.button5_Click);
            // 
            // sayi4Button
            // 
            this.sayi4Button.BackColor = System.Drawing.Color.Wheat;
            this.sayi4Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi4Button.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sayi4Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi4Button.Location = new System.Drawing.Point(12, 117);
            this.sayi4Button.Name = "sayi4Button";
            this.sayi4Button.Size = new System.Drawing.Size(48, 44);
            this.sayi4Button.TabIndex = 4;
            this.sayi4Button.Text = "4";
            this.sayi4Button.UseVisualStyleBackColor = false;
            this.sayi4Button.Click += new System.EventHandler(this.button6_Click);
            // 
            // eksiButton
            // 
            this.eksiButton.BackColor = System.Drawing.Color.Pink;
            this.eksiButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eksiButton.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eksiButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.eksiButton.Location = new System.Drawing.Point(174, 117);
            this.eksiButton.Name = "eksiButton";
            this.eksiButton.Size = new System.Drawing.Size(48, 44);
            this.eksiButton.TabIndex = 8;
            this.eksiButton.Text = "-";
            this.eksiButton.UseVisualStyleBackColor = false;
            // 
            // artiButton
            // 
            this.artiButton.BackColor = System.Drawing.Color.Pink;
            this.artiButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.artiButton.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.artiButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.artiButton.Location = new System.Drawing.Point(174, 67);
            this.artiButton.Name = "artiButton";
            this.artiButton.Size = new System.Drawing.Size(48, 44);
            this.artiButton.TabIndex = 7;
            this.artiButton.Text = "+";
            this.artiButton.UseVisualStyleBackColor = false;
            // 
            // boluButton
            // 
            this.boluButton.BackColor = System.Drawing.Color.Pink;
            this.boluButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.boluButton.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boluButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.boluButton.Location = new System.Drawing.Point(174, 217);
            this.boluButton.Name = "boluButton";
            this.boluButton.Size = new System.Drawing.Size(48, 44);
            this.boluButton.TabIndex = 16;
            this.boluButton.Text = "/";
            this.boluButton.UseVisualStyleBackColor = false;
            // 
            // carpiButton
            // 
            this.carpiButton.BackColor = System.Drawing.Color.Pink;
            this.carpiButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.carpiButton.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.carpiButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.carpiButton.Location = new System.Drawing.Point(174, 167);
            this.carpiButton.Name = "carpiButton";
            this.carpiButton.Size = new System.Drawing.Size(48, 44);
            this.carpiButton.TabIndex = 15;
            this.carpiButton.Text = "x";
            this.carpiButton.UseVisualStyleBackColor = false;
            // 
            // esittirButton
            // 
            this.esittirButton.BackColor = System.Drawing.Color.Blue;
            this.esittirButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.esittirButton.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.esittirButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.esittirButton.Location = new System.Drawing.Point(120, 217);
            this.esittirButton.Name = "esittirButton";
            this.esittirButton.Size = new System.Drawing.Size(48, 44);
            this.esittirButton.TabIndex = 14;
            this.esittirButton.Text = "=";
            this.esittirButton.UseVisualStyleBackColor = false;
            this.esittirButton.Click += new System.EventHandler(this.esittirButton_Click);
            // 
            // sayi0Button
            // 
            this.sayi0Button.BackColor = System.Drawing.Color.Wheat;
            this.sayi0Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi0Button.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sayi0Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi0Button.Location = new System.Drawing.Point(66, 217);
            this.sayi0Button.Name = "sayi0Button";
            this.sayi0Button.Size = new System.Drawing.Size(48, 44);
            this.sayi0Button.TabIndex = 13;
            this.sayi0Button.Text = "0";
            this.sayi0Button.UseVisualStyleBackColor = false;
            this.sayi0Button.Click += new System.EventHandler(this.button12_Click);
            // 
            // CButton
            // 
            this.CButton.BackColor = System.Drawing.Color.Red;
            this.CButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CButton.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.CButton.Location = new System.Drawing.Point(12, 217);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(48, 44);
            this.CButton.TabIndex = 12;
            this.CButton.Text = "C";
            this.CButton.UseVisualStyleBackColor = false;
            this.CButton.Click += new System.EventHandler(this.button13_Click);
            // 
            // sayi9Button
            // 
            this.sayi9Button.BackColor = System.Drawing.Color.Wheat;
            this.sayi9Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi9Button.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sayi9Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi9Button.Location = new System.Drawing.Point(120, 167);
            this.sayi9Button.Name = "sayi9Button";
            this.sayi9Button.Size = new System.Drawing.Size(48, 44);
            this.sayi9Button.TabIndex = 11;
            this.sayi9Button.Text = "9";
            this.sayi9Button.UseVisualStyleBackColor = false;
            this.sayi9Button.Click += new System.EventHandler(this.button14_Click);
            // 
            // sayi8Button
            // 
            this.sayi8Button.BackColor = System.Drawing.Color.Wheat;
            this.sayi8Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi8Button.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sayi8Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi8Button.Location = new System.Drawing.Point(66, 167);
            this.sayi8Button.Name = "sayi8Button";
            this.sayi8Button.Size = new System.Drawing.Size(48, 44);
            this.sayi8Button.TabIndex = 10;
            this.sayi8Button.Text = "8";
            this.sayi8Button.UseVisualStyleBackColor = false;
            this.sayi8Button.Click += new System.EventHandler(this.button15_Click);
            // 
            // sayi7Button
            // 
            this.sayi7Button.BackColor = System.Drawing.Color.Wheat;
            this.sayi7Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi7Button.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sayi7Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi7Button.Location = new System.Drawing.Point(12, 167);
            this.sayi7Button.Name = "sayi7Button";
            this.sayi7Button.Size = new System.Drawing.Size(48, 44);
            this.sayi7Button.TabIndex = 9;
            this.sayi7Button.Text = "7";
            this.sayi7Button.UseVisualStyleBackColor = false;
            this.sayi7Button.Click += new System.EventHandler(this.button16_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(243, 293);
            this.Controls.Add(this.boluButton);
            this.Controls.Add(this.carpiButton);
            this.Controls.Add(this.esittirButton);
            this.Controls.Add(this.sayi0Button);
            this.Controls.Add(this.CButton);
            this.Controls.Add(this.sayi9Button);
            this.Controls.Add(this.sayi8Button);
            this.Controls.Add(this.sayi7Button);
            this.Controls.Add(this.eksiButton);
            this.Controls.Add(this.artiButton);
            this.Controls.Add(this.sayi6Button);
            this.Controls.Add(this.sayi5Button);
            this.Controls.Add(this.sayi4Button);
            this.Controls.Add(this.sayi3Button);
            this.Controls.Add(this.sayi2Button);
            this.Controls.Add(this.sayi1Button);
            this.Controls.Add(this.ekranLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label ekranLabel;
        private System.Windows.Forms.Button sayi1Button;
        private System.Windows.Forms.Button sayi2Button;
        private System.Windows.Forms.Button sayi3Button;
        private System.Windows.Forms.Button sayi6Button;
        private System.Windows.Forms.Button sayi5Button;
        private System.Windows.Forms.Button sayi4Button;
        private System.Windows.Forms.Button eksiButton;
        private System.Windows.Forms.Button artiButton;
        private System.Windows.Forms.Button boluButton;
        private System.Windows.Forms.Button carpiButton;
        private System.Windows.Forms.Button esittirButton;
        private System.Windows.Forms.Button sayi0Button;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button sayi9Button;
        private System.Windows.Forms.Button sayi8Button;
        private System.Windows.Forms.Button sayi7Button;
    }
}

